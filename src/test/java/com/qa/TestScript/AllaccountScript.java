package com.qa.TestScript;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.qa.base.PredefineActions;

public class AllaccountScript extends PredefineActions {

	private WebDriver driver;
	private WebElement we;
	private String screenshotName;
	private String emailID;
	
	
	@BeforeMethod
	public void Setup() throws Exception {
		String driverPath = System.getProperty("user.dir") + "/src/test/java/Drivers/chromedriver.exe";
//		System.out.println(driverPath);
		System.setProperty("webdriver.chrome.driver","./src/main/resources/com/qa/driver/chromedriver.exe" );
		ChromeOptions options = new ChromeOptions();
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.get("http://test-dev.ap-southeast-2.elasticbeanstalk.com/login");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		String title = driver.getTitle();
		System.out.println(title);
		Assert.assertEquals("Login V2", title);

		
	}

	//Company Account
	@Test(priority = 1)
	public void createAccountForSingleUser1() throws Exception {

		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/" + dateName + ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);

		Assert.assertTrue(true);
		WebElement we = driver.findElement(By.xpath("//a[@id='txt2']"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//fieldset[@id='step1']//input[@name='next']"));
		we.click();

		String userName = "" + (int) (Math.random() * Integer.MAX_VALUE);
		String emailID = "User" + userName + "@example.com";
		System.out.println(emailID);
		we = driver.findElement(By.xpath("//input[@id='email']"));
		we.sendKeys(emailID);
		Thread.sleep(100);
		we = driver.findElement(By.xpath("//input[@id='password']"));
		we.sendKeys("Shreya@1");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step11']//input[@name='next']"));
		we.click();
		we = driver.findElement(By.xpath("//div[contains(text(),'Company Account')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='companyName']"));
		we.sendKeys("A");
		Thread.sleep(3000);
		we.clear();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='companyName']"));
		we.sendKeys("A");
		Thread.sleep(5000);
		we = driver.findElement(By.xpath("//a[contains(text(),'A LIMITED')]"));
		we.click();
		Thread.sleep(5000);
		we = driver.findElement(By.xpath("//fieldset[@id='step1']//input[@name='next']"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//a[@id='btn20']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//select[@class='selectoption option-add adjust-option directorTitle']/option[2]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//div[@class='row director-add removedirector']//input[@id='fname']"));
		we.sendKeys("Claura Adams");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//a[@class='this-btn']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step2']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='Date_of_Birth']"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[18]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[@class='ui-state-default']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step3']//select[@name='occupation']/option[45]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='cOccupation']"));
		we.sendKeys("Business Person");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step3']//div[@class='selected-flag']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//ul[@class='country-list']//span[@class='country-name'][contains(text(),'Australia')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='positionInCompany']/option[3]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step3']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='address']"));
		we.sendKeys("102 Hobson Street, Auckland CBD, Auckland, New Zealand");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='mobileNo']"));
		we.sendKeys("9729767778");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step4']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//li[1]//label[1]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='verificationa']"));
		we.sendKeys("000000");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step5']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='src_of_fund1']/option[1]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//input[@id='licence_last_name']"));
		we.sendKeys("s");
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//input[@id='licenseNumber']"));
		we.sendKeys("1234");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='licenseIssueDate']"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
		we.click();
		Thread.sleep(1000);
		
		
		we = driver.findElement(By.xpath("//input[@id='Expirydate']"));
		we.click();
		Thread.sleep(1000);
		
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[11]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
		we.click();
		Thread.sleep(1000);

		we = driver.findElement(By.xpath("//input[@id='versionNumber']"));
		we.sendKeys("1234");
		Thread.sleep(1000);
		
		
		we = driver.findElement(By.xpath("//fieldset[@id='step6']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='irdNumber']"));
		we.sendKeys("089559693");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step7']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step701']//input[@placeholder='Enter email address']"));
		we.sendKeys("michael@max.max");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath(
				"//fieldset[@id='step701']//span[@class='enter-btn-small small-padd1'][contains(text(),'Send them an email requesting info')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step701']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step12']//input[@name='previous']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath(
				"//fieldset[@id='step701']//span[@class='enter-btn-small small-padd'][contains(text(),'I will enter their details now')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step701']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@class='form-control input-field removedata more-director-address pac-target-input']"));
		we.sendKeys("102 Hobson Street, Auckland CBD, Auckland, New Zealand");
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//body/div[@class='container']/div[@class='row']/div[contains(@class,'col-md-12')]"
						+ "/div[@class='content_body']/form[@id='msform']/fieldset[@id='step702']/div[@class='content-section']"
						+ "/div[@class='input-content']/div[@class='row']/div[@class='col-sm-6 flag-drop']/div[@class='company-flag']/input[1]"));
		we.sendKeys("0128474798");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath(
				"//input[@class='input-field more-director-dob more-director-dateOfBirth hasDatepicker']"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[18]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[@class='ui-state-default']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step702']//select[@name='occupation']/option[4]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath(
				"//fieldset[@id='step702']//select[@class='selectoption form-group more-director-positionInCompany']/option[3]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step702']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//fieldset[@id='step703']//input[contains(@placeholder,'Enter licence number')]"));
		we.sendKeys("GSHDY678HJ");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath(
				"//input[@class='input-field issueDate lic_expiry_Date more-director-licenseIssueDate hasDatepicker']"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[81]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
		we.click();
		Thread.sleep(1000);
		
		
		we = driver.findElement(By.xpath(
				"//input[@class='input-field more-director-exp lic_expiry_Date more-director-licenseExpiryDate hasDatepicker']"));
		we.click();
		Thread.sleep(1000);
		
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[11]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step703']//input[@placeholder='Enter version number']"));
		we.sendKeys("002");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step703']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath(
				"//fieldset[@id='step704']//div[@class='col-sm-6 form-group country-set flag-drop']//div[@class='selected-flag']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//ul[@class='country-list']//span[@class='country-name'][contains(text(),'Australia')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step704']//input[@placeholder='XXX-XXX-XXX']"));
		we.sendKeys("022629743");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step704']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='pir']/option[2]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='comIRDNumber']"));
		we.sendKeys("010387051");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='sourceOfFunds']/option[4]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step12']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='isCompanyFinancialInstitution']/option[2]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='isCompanyActivePassive']/option[2]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='isCompanyUSCitizen']/option[2]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step13']//input[@name='next']"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//div[@class='textfirst']"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//li[contains(text(),'TSB Bank')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='nameOfAccount']"));
		we.sendKeys("A Limited co.");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='accountNumber']"));
		we.clear();
		we.sendKeys("0602410246335000");
		Thread.sleep(1000);
		
		JavascriptExecutor jsx1 = (JavascriptExecutor) driver;
		jsx1.executeScript("document.getElementById('attachbankfile').click()");
		Thread.sleep(5000);
		StringSelection selection = new StringSelection(
				"D:\\Office_Code_Base\\Mintcompany\\src\\main\\resources\\com\\qa\\driver\\IRF.jpg");
		Robot rb = new Robot();
		Thread.sleep(2000);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, null);
		Thread.sleep(2000);
		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_V);

		rb.keyRelease(KeyEvent.VK_V);
		rb.keyRelease(KeyEvent.VK_CONTROL);

		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//fieldset[@id='step14']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver
				.findElement(By.xpath("//label[contains(text(),'I have read and agree to the Terms and Conditions')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//label[contains(text(),'I am authorized to accept and act on behalf of all')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step15']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);	

		we = driver.findElement(By.xpath("//input[@id='submit']"));
		we.click();
		Thread.sleep(20000);
		
		System.out.println(emailID);
		driver.get("http://test-dev.ap-southeast-2.elasticbeanstalk.com/login?logout");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		we = driver.findElement(By.xpath("//button[@class='confirm']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='Email']"));
		we.clear();
		we.sendKeys(emailID);
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='password']"));
		we.clear();
		we.sendKeys("Shreya@1");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//button[@id='logbtn']"));
		we.click();

}
	
	//Individual account
	@Test(priority = 2)
	public void createAccountindividual() throws Exception {

		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/" + dateName + ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);

		Assert.assertTrue(true);
		WebElement we = driver.findElement(By.xpath("//a[@id='txt2']"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//fieldset[@id='step1']//input[@name='next']"));
		we.click();

		String userName = "" + (int) (Math.random() * Integer.MAX_VALUE);
		String emailID = "User" + userName + "@example.com";
		System.out.println(emailID);
		we = driver.findElement(By.xpath("//input[@id='email']"));
		we.sendKeys(emailID);
		Thread.sleep(100);
		we = driver.findElement(By.xpath("//input[@id='password']"));
		we.sendKeys("Shreya@1");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step11']//input[@name='next']"));
		we.click();
		we = driver.findElement(By.xpath("//div[contains(text(),'Individual Account')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='fullName']"));
		we.sendKeys("Zeena");
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//input[@id='surname']"));
		we.sendKeys("ali");
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//input[@id='dob']"));
		we.click();
		Thread.sleep(1000);
		

		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[18]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//a[@class='ui-state-default']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//div[@class='col-sm-6 form-group country-set flag-drop']//div[@class='selected-flag']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath(
				"//li[@class='country preferred active highlight']//span[@class='country-name'][contains(text(),'New Zealand')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//div[@class='container']//option[45]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='input-occupation']"));
		we.sendKeys("Software Testing");
		Thread.sleep(1000);
		JavascriptExecutor jsx = (JavascriptExecutor) driver;
		jsx.executeScript("window.scrollBy(0,1000)");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step3']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
        we = driver.findElement(By.xpath("//input[@id='address']"));
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("document.getElementById('address').value='102 Hobson Street, Auckland CBD, Auckland, New Zealand'");
		
		we.sendKeys("102 Hobson Street, Auckland CBD, Auckland, New Zealand");
		Thread.sleep(5000);
		we = driver.findElement(
				By.xpath("//div[@class='mobile_number first-mobile top-show']//div[@class='selected-flag']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath(
				"//ul[@class='country-list']//li[@class='country preferred']//span[@class='country-name'][contains(text(),'India')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='mobile']"));
		we.sendKeys("9729767778");
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//fieldset[@id='step4']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		//we = driver.findElement(By.xpath("//fieldset[@id='step5']//li[1]"));
		//we.click();

		we = driver.findElement(By.xpath("//input[@id='verification']"));
		we.sendKeys("000000");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step5']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='src_of_fund1']/option[2]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='passport_number']"));
		we.sendKeys("ANDHEH376");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='passport_issue_date']"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[81]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='dob2']"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[91]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//div[@class='row passport-select']//div[@class='selected-flag']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//ul[@class='country-list']//span[@class='country-name'][contains(text(),'Australia')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step6']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='Investors_Rate']/option[2]"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//input[@id='IRD_Number']"));
		we.sendKeys("030658442");
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//select[@id='wealth_src']/option[4]"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//select[@class='selectoption selectoption1 form-group']/option[2]"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//a[@id='add-country-another']"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//select[@class='selectoption selectoption1 form-group']/option[1]"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//fieldset[@id='step7']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//div[@class='textfirst']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//li[contains(text(),'ASB Bank')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='acount_holder_name']"));
		we.sendKeys("Zeena Ali");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='accountNumber']"));
		we.clear();
		we.sendKeys("1230860044496001");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@class=' checkname']"));
		we.sendKeys("D:\\Office_Code_Base\\MintCode\\src\\main\\resources\\com\\qa\\driver\\IRF.jpg");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step8']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//span[@class='checkmark']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step9']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='submit']"));
		we.click();
		Thread.sleep(20000);
		
		System.out.println(emailID);
		driver.get("http://test-dev.ap-southeast-2.elasticbeanstalk.com/login?logout");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		we = driver.findElement(By.xpath("//button[@class='confirm']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='Email']"));
		we.clear();
		we.sendKeys(emailID);
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='password']"));
		we.clear();
		we.sendKeys("Shreya@1");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//button[@id='logbtn']"));
		we.click();

	}
	//joint Account
			@Test(priority = 3)
			public void createAccountjoint() throws Exception {
				
				String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
				TakesScreenshot ts = (TakesScreenshot) driver;
				File source = ts.getScreenshotAs(OutputType.FILE);
				String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/" + dateName + ".png";
				File finalDestination = new File(destination);
				FileUtils.copyFile(source, finalDestination);

				Assert.assertTrue(true);
				WebElement we = driver.findElement(By.xpath("//a[@id='txt2']"));
				we.click();
				Thread.sleep(2000);
				we = driver.findElement(By.xpath("//fieldset[@id='step1']//input[@name='next']"));
				we.click();

				String userName = "" + (int) (Math.random() * Integer.MAX_VALUE);
				String emailID = "User" + userName + "@example.com";
				System.out.println(emailID);
				we = driver.findElement(By.xpath("//input[@id='email']"));
				we.sendKeys(emailID);
				Thread.sleep(100);
				we = driver.findElement(By.xpath("//input[@id='password']"));
				we.sendKeys("Shreya@1");
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step11']//input[@name='next']"));
				we.click();
				we = driver.findElement(By.xpath("//div[contains(text(),'Joint Account')]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step1']//input[@id='fullName']"));
				we.sendKeys("Mahesh");
				Thread.sleep(1000);
				
				we = driver.findElement(By.xpath("//input[@id='surname']"));
				we.sendKeys("Dubey");
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//input[@id='dob']"));
				we.click();
				Thread.sleep(1000);
				
				we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[18]"));
				we.click();
				Thread.sleep(1000);
				
				we = driver.findElement(By.xpath("//a[@class='ui-state-default']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step1']//div[@class='selected-flag']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step1']//li[14]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//select[@id='select-occupation']/option[7]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(
						By.xpath("//select[@class='selectoption form-group avisor-click working_with_adviser']/option[2]"));
				we.click();
				Thread.sleep(1000);
				
				we = driver.findElement(
						By.xpath("//select[@class='selectoption form-group avisor-click working_with_adviser']/option[2]"));
				we.click();
				Thread.sleep(1000);
				
				we = driver.findElement(
						By.xpath("//select[@id='isAdvisorCompany']/option[2]"));
				we.click();
				Thread.sleep(1000);
				
				we = driver.findElement(By.xpath("//input[@class='form-control input-field '] "));
				we.sendKeys("1000");
				Thread.sleep(1000);
				
				we = driver.findElement(By.xpath("//fieldset[@id='step1']//input[@name='next']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//input[@id='address']"));
				we.sendKeys("102 Hobson Street, Auckland CBD, Auckland, New Zealand");
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//div[@class='col-sm-6']//input[@id='mobileNo']"));
				we.sendKeys("8745246895");
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step2']//input[@name='next']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//li[1]//label[1]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//input[@id='verificationa']"));
				we.sendKeys("000000");
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step3']//input[@name='next']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//input[@id='license_number']"));
				we.sendKeys("HFJDJ647H");
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//input[@id='licenseIshueDate']"));
				we.click();
				Thread.sleep(1000);
				
				we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[81]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
				we.click();
				Thread.sleep(1000);
				
				we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
				we.click();
				Thread.sleep(1000);
				
				we = driver.findElement(By.xpath("//input[@id='dob1']"));
				we.click();
				Thread.sleep(1000);
				
				we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[11]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
				we.click();
				Thread.sleep(1000);
				
				
				we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
				we.click();
				Thread.sleep(1000);
				
				we = driver.findElement(
						By.xpath("//div[@class='row drivery-licence']//input[@placeholder='Enter version number']"));
				we.sendKeys("001");
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step4']//input[@name='next']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//select[@class='selectoption form-group aml-select PIR']/option[2]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step5']//input[@placeholder='XXX-XXX-XXX']"));
				we.sendKeys("036471441");
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//select[@id='wealth_src']/option[4]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//select[@id='isUSCitizen']/option[2]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step5']//input[@name='next']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//input[@class='fname input-field more-investor-fname']"));
				we.sendKeys("Another Joint");
				Thread.sleep(1000);
				
				we = driver.findElement(By.xpath("//input[@class='sname input-field more-investor-sname']"));
				we.sendKeys("Holder");
				Thread.sleep(1000);
				
				we = driver.findElement(By.xpath("//input[@placeholder='Enter email address']"));
				we.sendKeys("mno@pqr.co");
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//span[@class='enter-btn-small small-padd']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step6']//input[@name='next']"));
				we.click();
				Thread.sleep(2000);
				we = driver.findElement(By.xpath(
						"/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/form[1]/fieldset[7]/div[1]/div[2]/div[1]/div[4]/input[1]"));
				we.click();
				Thread.sleep(2000);
				
				we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[44]"));
				we.click();
				Thread.sleep(2000);
				
				we = driver.findElement(By.xpath("//a[@class='ui-state-default']"));
				we.click();
				Thread.sleep(2000);
				we = driver.findElement(By.xpath(
						"//div[@class='col-sm-6 form-group country-set flag-drop']//div[@class='intl-tel-input']//div[@class='intl-tel-input']//div[@class='selected-flag']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(
						By.xpath("//ul[@class='country-list']//span[@class='country-name'][contains(text(),'Australia')]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step601']//select[@name='occupation']/option[7]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step601']//input[@name='next']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath(
						"//fieldset[@id='step602']//button[@class='director-btn postal-address all-btn-color upper-btn same-as-investor1'][contains(text(),'Same as Investor 1')]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step602']//input[@placeholder='Enter mobile number']"));
				we.sendKeys("8456795213");
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step602']//input[@name='next']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(
						By.xpath("//fieldset[@id='step603']//input[contains(@placeholder,'Enter licence number')]"));
				we.sendKeys("BVCDGHV346G");
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//input[@class='input-field clear-more-investor more-investor-issue more-investor-licenseIssueDate hasDatepicker']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[81]"));
				we.click();
				Thread.sleep(1000);
				
				we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
				we.click();
				Thread.sleep(1000);
				
				we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
				we.click();
				Thread.sleep(1000);
				
				we = driver.findElement(By.xpath("//input[@class='input-field more-investor-exp clear-more-investor lic_expiry_Date more-investor-licenseExpiryDate hasDatepicker']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[11]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
				we.click();
				Thread.sleep(1000);
				
				we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step603']//input[@placeholder='Enter version number']"));
				we.sendKeys("002");
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step603']//input[@name='next']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath(
						"//fieldset[@id='step604']//select[@class='selectoption form-group aml-select presInvestRate']/option[2]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step604']//input[@placeholder='XXX-XXX-XXX']"));
				we.sendKeys("109045209");
				Thread.sleep(1000);
				we = driver.findElement(
						By.xpath("//fieldset[@id='step604']//select[@class='selectoption selectoption2 form-group usCitizen "
								+ "more-investor-usCitizen']/option[1]"));
				we.click();
				Thread.sleep(2000);
				we = driver.findElement(By.xpath("//fieldset[@id='step604']//input[@name='next']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//div[@class='textfirst']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//li[contains(text(),'ASB Bank')]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//input[@id='nameOfAccount']"));
				we.sendKeys("Abhay chobey");
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//input[@id='accountNumber']"));
				we.sendKeys("0201100317025000");
				Thread.sleep(2000);
				JavascriptExecutor jsx1 = (JavascriptExecutor) driver;
				jsx1.executeScript("document.getElementById('bank_document').click()");
				Thread.sleep(5000);
				StringSelection selection = new StringSelection(
						"C:\\Users\\Lenovo\\Mintproject\\Mintjointaccount\\src\\main\\resources\\com\\qa\\driver\\IRF.jpg");
				Robot rb = new Robot();
				Thread.sleep(2000);
				Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, null);
				Thread.sleep(2000);
				rb.keyPress(KeyEvent.VK_CONTROL);
				rb.keyPress(KeyEvent.VK_V);

				rb.keyRelease(KeyEvent.VK_V);
				rb.keyRelease(KeyEvent.VK_CONTROL);

				rb.keyPress(KeyEvent.VK_ENTER);
				rb.keyRelease(KeyEvent.VK_ENTER);
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step11']//input[@name='next']"));
				we.click();
				Thread.sleep(1000);
				we = driver
						.findElement(By.xpath("//label[contains(text(),'I have read and agree to the Terms and Conditions')]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(
						By.xpath("//label[contains(text(),'I am authorized to accept and act on behalf of all')]"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//fieldset[@id='step12']//input[@name='next']"));
				we.click();
				Thread.sleep(1000);	
				we = driver.findElement(By.xpath("//input[@id='submit']"));
				we.click();
				Thread.sleep(20000);
				
				System.out.println(emailID);
				driver.get("http://test-dev.ap-southeast-2.elasticbeanstalk.com/login?logout");
				driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

				we = driver.findElement(By.xpath("//button[@class='confirm']"));
				we.click();
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//input[@id='Email']"));
				we.clear();
				we.sendKeys(emailID);
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//input[@id='password']"));
				we.clear();
				we.sendKeys("Shreya@1");
				Thread.sleep(1000);
				we = driver.findElement(By.xpath("//button[@id='logbtn']"));
				we.click();

			}
			
			
	//trust Account
	@Test(priority = 4)
    public void createAccounttrust() throws Exception {
		
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/" + dateName + ".png";
		File finalDestination = new File(destination);
		FileUtils.copyFile(source, finalDestination);

		Assert.assertTrue(true);
		WebElement we = driver.findElement(By.xpath("//a[@id='txt2']"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//fieldset[@id='step1']//input[@name='next']"));
		we.click();

		String userName = "" + (int) (Math.random() * Integer.MAX_VALUE);
		String emailID = "User" + userName + "@example.com";
		System.out.println(emailID);
		we = driver.findElement(By.xpath("//input[@id='email']"));
		we.sendKeys(emailID);
		Thread.sleep(100);
		we = driver.findElement(By.xpath("//input[@id='password']"));
		we.sendKeys("Shreya@1");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step11']//input[@name='next']"));
		we.click();
		we = driver.findElement(By.xpath("//div[contains(text(),'Trust Account')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='companyName']"));
		we.sendKeys("IESL Trust");
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//input[@id='companyAddress']"));
		
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("document.getElementById('companyAddress').value='102 Hobson Street, Auckland CBD, Auckland, New Zealand'");
		
		we.sendKeys("102 Hobson Street, Auckland CBD, Auckland, New Zealand");
		Thread.sleep(5000);
		we = driver.findElement(By.xpath("//fieldset[@id='step1']//div[@class='selected-flag']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step1']//li[14]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='Date_of_incorporation']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//a[@class='ui-state-default ui-state-highlight ui-state-hover']"));
		we.click();
		Thread.sleep(1000);
		we = driver
				.findElement(By.xpath("//select[@class='selectoption form-group selectType typeOfTrust']/option[3]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='registrationNumber']"));
		we.sendKeys("HSJSG6788DG");
		Thread.sleep(1000);	
		JavascriptExecutor jsx1 = (JavascriptExecutor) driver;
		jsx1.executeScript("document.getElementById('attachdeedfile').click()");
		Thread.sleep(5000);
		StringSelection selection = new StringSelection(
				"D:\\Office_Code_Base\\Minttrust\\src\\main\\resources\\com\\qa\\driver\\IMG.jpg");
		Robot rb = new Robot();
		Thread.sleep(2000);
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, null);
		Thread.sleep(2000);
		rb.keyPress(KeyEvent.VK_CONTROL);
		rb.keyPress(KeyEvent.VK_V);

		rb.keyRelease(KeyEvent.VK_V);
		rb.keyRelease(KeyEvent.VK_CONTROL);

		rb.keyPress(KeyEvent.VK_ENTER);
		rb.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step1']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='fname']"));
		we.sendKeys("First User");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//a[@id='btn20']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//div[@class='row director-add removedirector']//input[@id='fname']"));
		we.sendKeys("Second User");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step2']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='Date_of_Birth']"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[43]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//a[contains(text(),'30')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step3']//select[@name='occupation']/option[6]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step3']//div[@class='selected-flag']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//ul[@class='country-list']//span[@class='country-name'][contains(text(),'Australia')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='positionInCompany']/option[3]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step3']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='address']"));
		we.sendKeys("102 Hobson Street, Auckland CBD, Auckland, New Zealand");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='mobileNo']"));
		we.sendKeys("9245862516");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step4']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//li[1]//label[1]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='verificationa']"));
		we.sendKeys("000000");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step5']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='licenseNumber']"));
		we.sendKeys("GHH67HDGHJ");
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//input[@id='licenseIssuedate']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[81]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
		we.click();
		Thread.sleep(1000);
		
		
		we = driver.findElement(By.xpath("//input[@id='Expirydate']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[11]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//input[@id='versionNumber']"));
		we.sendKeys("123");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step6']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='irdNumber']"));
		we.sendKeys("071211762");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step7']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step701']//input[@placeholder='Enter email address']"));
		we.sendKeys("shreya@12.co");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step701']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step702']//input[@id='address1']"));
		we.sendKeys("102 Hobson Street, Auckland CBD, Auckland, New Zealand");
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//body/div[@class='container']/div[@class='row']/div[contains(@class,'col-md-12')]"
						+ "/div[@class='content_body']/form[@id='msform']/fieldset[@id='step702']/div[@class='content-section']"
						+ "/div[@class='input-content']/div[@class='row']/div[@class='col-sm-6']"
						+ "/div[contains(@class,'mobile_number first-mobile mobile-index1')]/input[1]"));
		we.sendKeys("9451235896");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@class='input-field more-director-dob more-director-dateOfBirth hasDatepicker']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[43]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//a[@class='ui-state-default']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@class = 'Occupation selectoption selectOcc form-group OccupationOption more-director-select-occu']//option[5]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@class ='selectoption form-group more-director-positionInCompany']//option[2]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@class='next9 action-button']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath(
				"//fieldset[@id='step703']//select[@class='selectoption form-group Id_Type src_of_fund2']/option[2]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step703']//input[@placeholder='Enter passport number']"));
		we.sendKeys("HSJDJ3344J");
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath(
				"//input[@class='input-field issueDate pass_expiry more-director-passportIssueDate hasDatepicker']"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[81]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath(
				"//input[@class='input-field more-director-exp pass_expiry more-director-passportExpiryDate hasDatepicker']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[11]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
		we.click();
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//div[@class  "
				+ "='row passport-select1']//div[@class='selected-flag']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//ul[@class='country-list']//span[@class='country-name'][contains(text(),'Australia')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step703']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath(
				"//fieldset[@id='step704']//div[@class='col-sm-6 form-group country-set flag-drop']//div[@class='selected-flag']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//ul[@class='country-list']//span[@class='country-name'][contains(text(),'Australia')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step704']//input[@placeholder='XXX-XXX-XXX']"));
		we.sendKeys("073026644");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step704']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='pir']/option[2]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='comIRDNumber']"));
		we.sendKeys("015746335");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='sourceOfFunds']/option[3]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step12']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='isCompanyFinancialInstitution']/option[2]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='isCompanyActivePassive']/option[2]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//select[@id='isCompanyUSCitizen']/option[2]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step13']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//div[@class='textfirst']"));
		we.click();
		Thread.sleep(2000);
		we = driver.findElement(By.xpath("//li[contains(text(),'The Co-operative Bank')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='nameOfAccount']"));
		we.sendKeys("shreya");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='accountNumber']"));
		we.sendKeys("01100317025000");
		Thread.sleep(1000);
		JavascriptExecutor jsx11 = (JavascriptExecutor) driver;
		jsx11.executeScript("document.getElementById('attachbankfile').click()");
		Thread.sleep(5000);
		StringSelection selection11 = new StringSelection(
				"D:\\Office_Code_Base\\MintProject\\src\\main\\resources\\com\\qa\\driver\\IRF.jpg");
		Robot rb1 = new Robot();
		Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, null);
		rb1.keyPress(KeyEvent.VK_CONTROL);
		rb1.keyPress(KeyEvent.VK_V);
		Thread.sleep(1000);
		rb1.keyRelease(KeyEvent.VK_V);
		rb1.keyRelease(KeyEvent.VK_CONTROL);
		Thread.sleep(1000);
		rb1.keyPress(KeyEvent.VK_ENTER);
		rb1.keyRelease(KeyEvent.VK_ENTER);
		Thread.sleep(3000);
		
		we = driver.findElement(By.xpath("//input[@class='next14 action-button']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//label[contains(text(),'I have read and agree to the Terms and Conditions')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(
				By.xpath("//label[contains(text(),'I am authorized to accept and act on behalf of all')]"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//fieldset[@id='step15']//input[@name='next']"));
		we.click();
		Thread.sleep(1000);	
		we = driver.findElement(By.xpath("//input[@id='submit']"));
		we.click();
		Thread.sleep(20000);
		
		System.out.println(emailID);
		driver.get("http://test-dev.ap-southeast-2.elasticbeanstalk.com/login?logout");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

		we = driver.findElement(By.xpath("//button[@class='confirm']"));
		we.click();
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='Email']"));
		we.clear();
		we.sendKeys(emailID);
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='password']"));
		we.clear();
		we.sendKeys("Shreya@1");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//button[@id='logbtn']"));
		we.click();

				}

	
	//Minor Account
		@Test(priority = 5)
		public void createAccountMinor() throws Exception {
		
			String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
			TakesScreenshot ts = (TakesScreenshot) driver;
			File source = ts.getScreenshotAs(OutputType.FILE);
			String destination = System.getProperty("user.dir") + "/FailedTestsScreenshots/" + dateName + ".png";
			File finalDestination = new File(destination);
			FileUtils.copyFile(source, finalDestination);

			Assert.assertTrue(true);
			WebElement we = driver.findElement(By.xpath("//a[@id='txt2']"));
			we.click();
			Thread.sleep(2000);
			we = driver.findElement(By.xpath("//fieldset[@id='step1']//input[@name='next']"));
			we.click();

			String userName = "" + (int) (Math.random() * Integer.MAX_VALUE);
			String emailID = "User" + userName + "@example.com";
			System.out.println(emailID);
			we = driver.findElement(By.xpath("//input[@id='email']"));
			we.sendKeys(emailID);
			Thread.sleep(100);
			we = driver.findElement(By.xpath("//input[@id='password']"));
			we.sendKeys("Shreya@1");
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//fieldset[@id='step11']//input[@name='next']"));
			we.click();
			we = driver.findElement(By.xpath("//div[contains(text(),'Minor Account')]"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//input[@id='fullName']"));
			we.sendKeys("sandhya");
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//input[@id='surname']"));
			we.sendKeys("Rao");
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//input[@id='dob']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[18]"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//a[@class='ui-state-default']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//fieldset[@id='step3']//div[@class='selected-flag']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(
					By.xpath("//ul[@class='country-list']//span[@class='country-name'][contains(text(),'Australia')]"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//select[@name='occupation']/option[18]"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//fieldset[@id='step3']//input[@name='next']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//input[@id='address']"));
			we.sendKeys("102 Hobson Street, Auckland CBD, Auckland, New Zealand");
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//input[@id='mobile']"));
			we.sendKeys("8546247896");
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//fieldset[@id='step4']//input[@name='next']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//li[1]//label[1]"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//input[@id='verification']"));
			we.sendKeys("000000");
			Thread.sleep(4000);
			we = driver.findElement(By.xpath("//fieldset[@id='step5']//input[@name='next']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//select[@id='src_of_fund1']/option[2]"));
			we.click();
			Thread.sleep(2000);
			we = driver.findElement(By.xpath("//div[@class='row passport-select']//input[@id='passport_number']"));
			we.sendKeys("LZ115041");
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//input[@class='passport_issue_Date input-field issuedate hasDatepicker']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[81]"));
			we.click();
			Thread.sleep(1000);
			
			we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
			we.click();
			Thread.sleep(1000);
			
			we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
			we.click();
			Thread.sleep(1000);
			
			we = driver.findElement(By.xpath("//input[@id='dob2']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[11]"));
			we.click();
			Thread.sleep(1000);
			
			we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
			we.click();
			Thread.sleep(1000);
			
			we = driver.findElement(By.xpath("//a[contains(text(),'29')]"));
			we.click();
			Thread.sleep(1000);
			
			JavascriptExecutor jsx = (JavascriptExecutor) driver;
			jsx.executeScript("window.scrollBy(0,1000)");
			we = driver.findElement(By.xpath("//div[@class='row passport-select']//div[@class='selected-flag']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(
					By.xpath("//ul[@class='country-list']//span[@class='country-name'][contains(text(),'Australia')]"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//fieldset[@id='step6']//input[@name='next']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//select[@id='Investors_Rate']/option[3]"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//input[@id='IRD_Number']"));
			we.sendKeys("048448984");
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//select[@id='wealth_src']/option[3]"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//fieldset[@id='step7']//input[@name='next']"));
			we.click();
			Thread.sleep(1000);
			// we =
			// driver.findElement(By.xpath("//select[@id='minor_title']/option[3]"));
			// we.click();
			// Thread.sleep(1000);
			we = driver.findElement(By.xpath("//input[@id='minorfullName']"));
			we.sendKeys("Ashwin");
			we = driver.findElement(By.xpath("//input[@id='minordob']"));
			we.click();
			Thread.sleep(1000);
			
			we = driver.findElement(By.xpath("//input[@id='minorSurName']"));
			we.sendKeys("Jain");
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//input[@class='input-field minordob hasDatepicker']"));
			we.click();
			Thread.sleep(1000);
			
			we = driver.findElement(By.xpath("//select[@class='ui-datepicker-year']/option[85]"));
			we.click();
			Thread.sleep(1000);
			
			we = driver.findElement(By.xpath("//select[@class='ui-datepicker-month']/option[3]"));
			we.click();
			Thread.sleep(1000);
			
			we = driver.findElement(By.xpath("//a[contains(text(),'1')]"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//fieldset[@id='step8']//div[@class='selected-flag']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(
					By.xpath("//ul[@class='country-list']//span[@class='country-name'][contains(text(),'Australia')]"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(
					By.xpath("//select[@class='OccupationMinor selectoption  otherOcc  form-group']/option[6]"));
			we.click();
			Thread.sleep(1000);

			we = driver.findElement(By.xpath("//a[@id='same-as-investor1']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//fieldset[@id='step8']//input[@name='next']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//select[@id='src_of_fund2']/option[1]"));
			we.click();
			Thread.sleep(1000);
			JavascriptExecutor jsx1 = (JavascriptExecutor) driver;
			jsx1.executeScript("document.getElementById('minor_birth_certificate').click()");
			Thread.sleep(5000);
			StringSelection selection = new StringSelection(
					"D:\\Office_Code_Base\\MintProject\\src\\main\\resources\\com\\qa\\driver\\IRF.jpg");
			Robot rb = new Robot();
			Thread.sleep(2000);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, null);
			Thread.sleep(2000);
			rb.keyPress(KeyEvent.VK_CONTROL);
			rb.keyPress(KeyEvent.VK_V);

			rb.keyRelease(KeyEvent.VK_V);
			rb.keyRelease(KeyEvent.VK_CONTROL);

			rb.keyPress(KeyEvent.VK_ENTER);
			rb.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//fieldset[@id='step9']//input[@name='next']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//input[@id='minor_IRD_Number']"));
			we.sendKeys("025317491");
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//fieldset[@id='step10']//input[@name='next']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//div[@class='textfirst']"));
			we.click();
			Thread.sleep(2000);
			we = driver.findElement(By.xpath("//li[contains(text(),'Kiwibank')]"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//input[@id='acount_holder_name']"));
			we.sendKeys("Zain imam");
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//input[@id='accountNumber']"));

			we.sendKeys("9005-0764860-001");
			Thread.sleep(1000);
			JavascriptExecutor jsx11 = (JavascriptExecutor) driver;
			jsx11.executeScript("document.getElementById('attachbankfile').click()");
			Thread.sleep(5000);
			StringSelection selection1 = new StringSelection(
					"D:\\Office_Code_Base\\MintProject\\src\\main\\resources\\com\\qa\\driver\\IRF.jpg");
			Robot rb1 = new Robot();
			Thread.sleep(2000);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(selection, null);
			Thread.sleep(2000);
			rb1.keyPress(KeyEvent.VK_CONTROL);
			rb1.keyPress(KeyEvent.VK_V);

			rb1.keyRelease(KeyEvent.VK_V);
			rb1.keyRelease(KeyEvent.VK_CONTROL);

			rb1.keyPress(KeyEvent.VK_ENTER);
			rb1.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//fieldset[@id='step11']//input[@name='next']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//span[@class='checkmark']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//fieldset[@id='step12']//input[@name='next']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//input[@id='submit']"));
			we.click();
			Thread.sleep(20000);
			
			System.out.println(emailID);
			driver.get("http://test-dev.ap-southeast-2.elasticbeanstalk.com/login?logout");
			driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);

			we = driver.findElement(By.xpath("//button[@class='confirm']"));
			we.click();
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//input[@id='Email']"));
			we.clear();
			we.sendKeys(emailID);
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//input[@id='password']"));
			we.clear();
			we.sendKeys("Shreya@1");
			Thread.sleep(1000);
			we = driver.findElement(By.xpath("//button[@id='logbtn']"));
			we.click();

		}
	@AfterMethod
	public void teardown() throws InterruptedException
	{
		
		String title1 = driver.getTitle();
		System.out.println(title1);
		// Assert.assertEquals(title, "Beneficiary Dashboard");

		if (title1 == "Beneficiary Dashboard") {
			System.out.println(" Account" + emailID + "Successful");
		} else {
			System.out.println(" Account" + emailID + "UnSuccessful");
		}
		driver.quit();
	}
}
